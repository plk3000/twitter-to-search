/**
 * Created by jsolis on 3/17/2015.
 */

var Twitter = require('twitter');

var twitterClient = new Twitter({
	consumer_key: 'j4Flj37XdQ7iHY5dfCvx2nZIZ',
	consumer_secret: 'CTgtxiwBCBgmQXTK9RYOGnEoc78SVPK26dwzPWJkBZckdtG98r',
	access_token_key: '562401596-bvnQWdF5rBvXtplkfQwwonqdpF4roSH2Av8RUueg',
	access_token_secret: 'GPRbHXdTy1P6TvGbgVvJOHFDDvxhtWLZ44tHsKZSXuaYE'
});

/*var params = {*//*screen_name: 'nodejs'*//*};
twitterClient.get('statuses/home_timeline', params, function(error, tweets, response){
	if (!error) {
		console.log(tweets);
	}
});*/

/*var helios = require('helios');
var solr_client = new helios.client({
	host : 'localhost', // Insert your client host
	port : 8989,
	path : '/solr', // Insert your client solr path
	timeout : 1000  // Optional request timeout
});*/

// Use `var solr = require('solr-client')` in your code
var solr = require('solr-client');

// Create a client
var client = solr.createClient();

// Switch on "auto commit", by default `client.autoCommit = false`
client.autoCommit = true;

client.delete('id','*',function(err,obj){
	if(err){
		console.log(err);
	}else{
		console.log(obj);
	}
	client.softCommit(function(err,res){
		if(err){
			console.log(err);
		}else{
			console.log(res);
		}
		start();
	});
});

// Add documents
/*client.add(docs,function(err,obj){
	if(err){
		console.log(err);
	}else{
		console.log(obj);
	}
});*/

//solr-client
var sendToSolr = function (tweet) {
	var date = new Date(tweet.created_at);
	var solrdoc = {};
	try {
		solrdoc.id = tweet.id;
		solrdoc.screen_name = tweet.user.screen_name;
		solrdoc.type = 'node';
		solrdoc.timestamp = date.toISOString();
		solrdoc.lang = tweet.lang;
		solrdoc.favourites_count = tweet.favorite_count;
		if(tweet.lang === 'en')
			solrdoc.text = tweet.text;
		else
			solrdoc.text_fr = tweet.text;
		client.add(solrdoc, function (err, obj) {
			if (err) {
				console.log('------------------BEGIN SOLR ERROR------------------');
				console.log(err);
				console.log('------------------END SOLR ERROR------------------');
			}else{
				//console.log(obj);
			}
		});
	} catch (err) {
		console.log(err);
	}
};

//Helios
/*var sendToSolr = function (tweet) {
	var date = new Date(tweet.created_at);
	var solrdoc = new helios.document();
	try {
		solrdoc.addField('id', tweet.id);
		solrdoc.addField('screen_name', tweet.user.screen_name);
		solrdoc.addField('type', 'node');
		solrdoc.addField('timestamp', date.toISOString());
		solrdoc.addField('lang', tweet.lang);
		solrdoc.addField('favourites_count', 'value2');
		solrdoc.addField('field_name2', tweet.favorite_count);
		solr_client.addDoc(solrdoc, true, function (err) {
			console.log('------------------BEGIN SOLR ERROR------------------');
			if (err) console.log(err);
			console.log('------------------END SOLR ERROR------------------');
		});
	} catch (err) {
		console.log(err);
	}
};*/
var start = function () {
	/**
	 * Stream statuses filtered by keyword
	 * number of tweets per second depends on topic popularity
	 **/
//twitterClient.stream('statuses/filter', {track: 'solr'},  function(stream){
	twitterClient.stream('statuses/sample', {}, function (stream) {
		stream.on('data', function (tweet) {
			/*console.log('------------------BEGIN TWEET------------------');
			 console.log(tweet);
			 console.log('------------------END TWEET------------------');*/
			if (tweet.user && (tweet.lang === 'fr' || tweet.lang === 'en')) {
				sendToSolr(tweet);
			}
		});

		stream.on('error', function (error) {
			/*console.log('------------------BEGIN TWITTER ERROR------------------');
			 console.log(error);
			 console.log('------------------END TWITTER ERROR------------------');*/
		});
	});
};