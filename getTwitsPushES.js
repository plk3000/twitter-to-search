/**
 * Created by jsolis on 3/17/2015.
 */

var Twitter = require('twitter'),
	http = require('http'),
	querystring = require('querystring');

var twitterClient = new Twitter({
	consumer_key: 'j4Flj37XdQ7iHY5dfCvx2nZIZ',
	consumer_secret: 'CTgtxiwBCBgmQXTK9RYOGnEoc78SVPK26dwzPWJkBZckdtG98r',
	access_token_key: '562401596-bvnQWdF5rBvXtplkfQwwonqdpF4roSH2Av8RUueg',
	access_token_secret: 'GPRbHXdTy1P6TvGbgVvJOHFDDvxhtWLZ44tHsKZSXuaYE'
});

var post_options = {
	host: 'localhost',
	port: '9200',
	path: '/twitter/tweets',
	method: 'POST'//,
	//headers: {
		//'Content-Type': 'application/x-www-form-urlencoded',
		//'Content-Length': post_data.length
	//}
};

var sendToES = function (tweet) {
	var date = new Date(tweet.created_at);
	var solrdoc = {};

	var post_req = http.request(post_options, function(res) {
		res.setEncoding('utf8');
		res.on('data', function (chunk) {
			console.log('Response: ' + chunk);
		});
	});
	post_req.on('error', function(e) {
		console.log('problem with request: ' + e.message);
	});
	try {
		solrdoc.id = tweet.id;
		solrdoc.screen_name = tweet.user.screen_name;
		solrdoc.type = 'node';
		solrdoc.timestamp = date.toISOString();
		solrdoc.lang = tweet.lang;
		solrdoc.favourites_count = tweet.favorite_count;
		if(tweet.lang === 'en')
			solrdoc.text = tweet.text;
		else
			solrdoc.text_fr = tweet.text;
		post_req.write(JSON.stringify(solrdoc));
		post_req.end();
		/*client.add(solrdoc, function (err, obj) {
			if (err) {
				console.log('------------------BEGIN SOLR ERROR------------------');
				console.log(err);
				console.log('------------------END SOLR ERROR------------------');
			}else{
				//console.log(obj);
			}
		});*/
	} catch (err) {
		console.log(err);
	}
};

var start = function () {
	/**
	 * Stream statuses filtered by keyword
	 * number of tweets per second depends on topic popularity
	 **/
//twitterClient.stream('statuses/filter', {track: 'solr'},  function(stream){
	twitterClient.stream('statuses/sample', {}, function (stream) {
		stream.on('data', function (tweet) {
			/*console.log('------------------BEGIN TWEET------------------');
			 console.log(tweet);
			 console.log('------------------END TWEET------------------');*/
			if (tweet.user && (tweet.lang === 'fr' || tweet.lang === 'en')) {
				sendToES(tweet);
				count++;
			}
		});

		stream.on('error', function (error) {
			/*console.log('------------------BEGIN TWITTER ERROR------------------');
			 console.log(error);
			 console.log('------------------END TWITTER ERROR------------------');*/
		});
	});
};

start();